# Clean the memory from all objects
rm(list=ls())

library("ggplot2")
library("ggpubr")
library(scales)

library(gridExtra)
library(grid)


kranks <- c(1, 5, 10, 25, 50, 100)

# Do it for k = 1, 5, 10, 25, 50, 100

isax.filenames <- c("results/isax-time-stopping-0.05-1.csv",
                    "results/isax-time-stopping-0.05-5.csv",
                    "results/isax-time-stopping-0.05-10.csv",
                    "results/isax-time-stopping-0.05-25.csv",
                     "results/isax-time-stopping-0.05-50.csv",
                     "results/isax-time-stopping-0.05-100.csv")

dstree.filenames <- c("results/dstree-time-stopping-0.05-1.csv",
                    "results/dstree-time-stopping-0.05-5.csv",
                    "results/dstree-time-stopping-0.05-10.csv",
                    "results/dstree-time-stopping-0.05-25.csv",
                     "results/dstree-time-stopping-0.05-50.csv",
                     "results/dstree-time-stopping-0.05-100.csv")

getResults <- function(prefix, filename){
    data <- read.csv(file=filename, header=TRUE, sep=",")
    
    data$exact <- (data$eq == 0)
    data$eq05 <- (data$eq > 0.05)
    data$eq01 <- (data$eq > 0.01)
    
    results <- aggregate(.~k+dataset, data = data, FUN = mean)[c(1,2,6,7, 9, 10, 11, 12)]
    
    names(results)[5] <- "timePercent"
    results$index <- paste(prefix, sub(".*/", "", sub("-.*", "", filename)), sep ="")
    
    results
}

data <- NA

for(i in 1:length(isax.filenames)){
    data_ <- rbind(getResults("2.", dstree.filenames[i]), getResults("1.", isax.filenames[i]))
    data_$krank <- kranks[i]

    if(is.na(data)) data <- data_
    else {
        data <- rbind(data, data_)
    }
}

##################################################################
##################################################################
##################################################################

cbPalette <- c( "#0072B2",  
    "red", # This is for physionet 
    "#D55E00", "#E69F00","#009E73")
#cbPalette <- c( "#0072B2",  "#E69F00","#009E73")
shapes <- c(1,
        4, #This is for physionet 
        2, 3, 8)

plot1 <- ggplot(data = data, aes(x=factor(krank), y=100*exact, group=dataset)) +
    geom_point(size = 2, aes(color=dataset, shape=dataset)) +
    geom_line(aes(color=dataset)) +
    facet_wrap(~index, ncol=2) +
    scale_shape_manual(values = shapes) +
    theme_bw() +
    theme( legend.position = "none", legend.title = element_blank(), plot.margin = unit(c(.2, .2, .15, .1), "cm")) +
    xlab("k-rank") +
    ylab("Exact Answers (%)") +
    ylim(90, 100) +
    scale_size_manual(values=c(2,.5,.5,.5,.5,.5)) +
    scale_x_discrete(labels=c("1", "5", "10", "25", "50", "100")) +
    scale_colour_manual(values=cbPalette) +
    ggtitle("time-bound criterion")

plot2 <- ggplot(data = data, aes(x=factor(krank), y=(1-timePercent), group=dataset)) +
    geom_point(size = 2, aes(color=dataset, shape=dataset)) +
    geom_line(aes(color=dataset)) +
    facet_wrap(~index, ncol=2) +
    scale_shape_manual(values = shapes) +
    theme_bw() +
    theme( legend.position = "none", legend.title = element_blank(),
    plot.margin = unit(c(.2, .2, .15, .1), "cm")) +
    xlab("k-rank") +
    ylab("Time Savings (%)") +
    scale_size_manual(values=c(2,.5,.5,.5,.5,.5)) +
    scale_x_discrete(labels=c("1", "5", "10", "25", "50", "100")) +
   # scale_x_continuous(breaks = c(0, 25, 50, 75, 100), labels=c("0", "25", "50", "75", "100")) +
    scale_y_continuous(breaks = c(0, .2, .4, .6, .8, 1), labels = c("0", "20", "40", "60", "80", "100"), limits = c(0, 1)) +
    scale_colour_manual(values=cbPalette) +
    ggtitle("time-bound criterion")
