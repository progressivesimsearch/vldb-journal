# Created by Theophanis Tsandilas, Inria
# Stopping based on Quantile regression to assess an upper bound for the time to find the exact answer (1NN)

# Clean the memory from all objects
rm(list=ls())

require("quantreg")

# General process.
#  1000 or 5000 series are used as the pool of training and testing queries (T = R + S)
# Size of S (testing queries per iteration) N.test = 200 (constant)
# Size of R (training queries) k = 50, 100, 200, 500
# We follow a Monte Carlo validation approach that repeats  N = 100 times the following steps:
# 1. We randomly draw a sample (from T) for the testing set S
# 2. From the remaining series, we randomly draw a sample of size k for the training set R
# 3. We train the model and then test it
# 4. We repeat

datasets <- c("seismic", "sald", "deep1b", "synthetic", "physionet")
algorithms <- c("isax", "dstree")

N.test <- 160
k <- 100 # Number of training queries
N <- 100 # Testing iterations

# K-NN, rank of NN
Ks <- c(1, 5, 10, 25, 50, 100)
#Ks <- c(1, 10, 20, 30, 40, 50)

phi <- 0.05 # To vary for other phi values
# phi <- 0.05
# phi <- 0.10

summary.table <- function(data.bsf){
    data.bsf <- data.bsf[data.bsf$leavesVisited > 0,]
    data.bsf <- data.bsf[,c(2,4,5,6,7,10,11)]
   
    # data.bsf$time <- log2(data.bsf$time)
    #data.bsf$totalTime <- log2(data.bsf$totalTime)
   # data.bsf$leavesVisited <- log2(data.bsf$leavesVisited)
   # data.bsf$totalLeavesVisited <- log2(data.bsf$totalLeavesVisited)
    
    # Attention! Remove this if you really want prediction to be performed on the number of visited leaves
    # I have this just for practical reasons to make comparions based on time rather than number of leaves
    data.bsf$leavesVisited <- log2(data.bsf$time)
    data.bsf$totalLeavesVisited <- log2(data.bsf$totalTime)
    
    data.bsf$distance <- sqrt(data.bsf$distance)

    #best <- data.bsf[data.bsf$answerId=="bestNN",]
    #merge(data.bsf, best, by="queryId")#[,c(1,2,4,5,6,7,3,9)]

    best <- data.bsf[data.bsf$answerId=="bestNN",]
    merge(data.bsf, best, by="queryId")[,c(1,2,4,5,6,7,3,9)]
}

getTrainingFrame <- function(data){
    appr <- aggregate(distance.x ~ queryId, data = data, max)
    best <- data[data$answerId.x=="bestNN",]
    merge(appr, best, by="queryId")
}


pi <- function(dist, lmodel, level = 0.95) {
    newdata = data.frame(distance.x = dist)
    pi <- predict(lmodel, newdata, interval="predict", level = level)
    
    c(pi[1], max(0, pi[2]), min(dist, pi[3]))
}

for(K in Ks){
    for(algorithm in algorithms){
        prefix <- paste("../datasets/search-ed/", algorithm, "/", sep="")
        csvfile <- paste("output/", algorithm, "-time-stopping-", phi, "-", K, "SALD.csv", sep="")
        
        results <- data.frame(k = integer(), dataset = character(), i = integer(), qid = integer(), time = double(), totalTime =  double(), eq = double(), ratio = double(), stringsAsFactors=FALSE)
        
        for(dataset in datasets){
        #    error <- 0

            cat(k, ": loading ", dataset, "\n")
            
            # Data preparation #################################################
            queries.file <- paste(prefix, dataset, ".csv", sep="")
            data.bsf <- read.csv(file=queries.file, header=TRUE, sep=",")
            
            data.bsf <- data.bsf[data.bsf$NNrank==K,]
            # Data preparation #################################################
            queries <- summary.table(data.bsf)
            bad_ids <- unique(queries[queries$distance.y == 0,]$queryId)
            queries <- queries[!(queries$queryId %in% bad_ids),]
            #queries <- queries[queries$leavesVisited.x > 0,]
            
            # Measures: We measure coverage, range, and MSE for 10 different ponts in time
            # represented by the logarithm of the number of visited leaves: 1, 2,..., 10
        
            # Also add time and num of leaves
        
            for(i in 1:N){ cat("iteration: ", i, "\n") ###################
                testing.ids <- sample(queries$queryId, N.test)
                training.ids <- sample(queries$queryId[-testing.ids], k)
                
                testing <- queries[queries$queryId %in% testing.ids,]
                training <- queries[queries$queryId %in% training.ids,]
            
                training <- getTrainingFrame(training)[c(2,6)]
                names(training) <- c("distance", "leavesVisited")
                
                #lmodel <- lm(leavesVisited ~ distance, data = training)
                lmodel <- rq(leavesVisited ~ distance, data = training, tau = 1 - phi)
                slope <- lmodel$coefficients[[2]]
                intercept <- lmodel$coefficients[[1]]
                
                # And now do the testing!
                for(qid in testing.ids){
                    query <- testing[testing$queryId == qid,]
                    
                    predict = query[1,]$distance.x * slope + intercept
                    
                    #distance <- query[1,]$distance.x
                    #PI <- pi(distance, lmodel, level = 0.9)
                    
                    t <- 2^predict
                    
                    for(j in 1:nrow(query)){
                        line <- query[j,]
                        T <- 2^line$totalLeavesVisited.x
                        t <- min(t, T)
                        #print(line)
                        
                        if(line$leavesVisited.x > predict){
                            if(j == 1){
                                dist <- line$distance.x
                                t <- 2^line$leavesVisited.x
                            }
                            else {
                                dist <- query[j -1,]$distance.x
                            }
                            # STOP here....
                            eq <- (dist / line$distance.y) - 1
                            results[nrow(results) + 1, ]  <- list(k, dataset, i, qid, t, T, eq, t/T)

                            break;
                        } else if(line$answerId.x == "bestNN"){
                            results[nrow(results) + 1, ]  <- list(k, dataset, i, qid, t, T, 0, t/T)
                        }
                    }
                    
                }
            }
        }

        write.csv(results, file=csvfile)
    }
}