# Author: Theophanis Tsandilas, Inria

# Clean the memory from all objects
rm(list=ls())

library("ggplot2")
library(scales)
library(gridExtra)

#cbPalette <- c("#E69F00", "#D55E00", "#009E73", "#0072B2",   "#CC79A7")
cbPalette <- c("#E69400", "#009E73", "#0072B2",   "#CC79A7")

filename <- "data/accuracy_G_nosample_0.950000_G.csv"
data1 <- read.csv(file=filename, header=TRUE, sep=",")

data1$coverage <- 100*data1$coverage

plot1 <- ggplot(data = data1, aes(x=factor(data1$size), y=data1$coverage)) +
  geom_point(size = 2, aes(shape=data1$dataset, colour=data1$dataset)) +
  geom_line(aes(colour=data1$dataset, group=data1$dataset)) +
  scale_shape_manual(values = c(0, 2, 8, 15, 19)) + 
  theme_bw() + 
  theme(legend.position = "none", legend.title = element_blank(), plot.margin=unit(c(0.2,0,0.2,0.1),"cm")) +
  xlab("Dataset Cardinality") +
  ylab("Coverage (%)") + 
  ylim(0, 80) +
  scale_size_manual(values=c(2,.5,.5,.5,.5,.5)) + 
  scale_colour_manual(values=cbPalette) +
  scale_x_discrete(labels = c("1K", "10K", "50K", "100K"))

#################################################################

filename <- "data/accuracy_G_sample_0.950000_G.csv"
data2 <- read.csv(file=filename, header=TRUE, sep=",")

data2$coverage <- 100*data2$coverage

plot2 <- ggplot(data = data2, aes(x=factor(data2$size), y=data2$coverage)) +
geom_point(size = 2, aes(shape=data2$dataset, colour=data2$dataset)) +
geom_line(aes(colour=data2$dataset, group=data2$dataset)) +
scale_shape_manual(values = c(0, 2, 8, 15, 19)) +
theme_bw() +
theme(axis.title.y=element_blank(), legend.position = "none", legend.title = element_blank(), plot.margin=unit(c(0.2,0.3,0.2,0),"cm"), axis.ticks.y=element_blank(), axis.text.y = element_blank()) +
xlab("Dataset Cardinality") +
ylab("Coverage (%)") +
ylim(0, 80) +
scale_size_manual(values=c(2,.5,.5,.5,.5,.5)) +
scale_colour_manual(values=cbPalette) +
scale_x_discrete(labels = c("1K", "10K", "50K", "100K"))

#################################################################

filename <- "data/accuracy_Gq_random_0.950000_BESTEXP_Gq.csv"
data3 <- read.csv(file=filename, header=TRUE, sep=",")
data3 <- data3[data3$size != 500000,]

data3$coverage <- 100*data3$coverage

plot3 <- ggplot(data = data3, aes(x=factor(data3$size), y=data3$coverage)) +
geom_point(size = 2, aes(shape=data3$dataset, colour=data3$dataset)) +
geom_line(aes(colour=data3$dataset, group=data3$dataset)) +
scale_shape_manual(values = c(0, 2, 8, 15, 19)) +
theme_bw() +
theme(axis.title.y=element_blank(), legend.position = "none", legend.title = element_blank(), plot.margin=unit(c(0.2,0,0.2,0),"cm")) +
xlab("Dataset Cardinality") +
ylab("Coverage (%)") +
ylim(0, 80) +
scale_size_manual(values=c(2,.5,.5,.5,.5,.5)) +
scale_colour_manual(values=cbPalette) +
scale_x_discrete(labels = c("10K", "50K", "100K", "1M"))


#################################################################

filename <- "data/accuracy_Gq_GNAT_0.950000_BESTEXP_Gq.csv"
data4 <- read.csv(file=filename, header=TRUE, sep=",")
data4 <- data4[data4$size != 500000,]

data4$coverage <- 100*data4$coverage

plot4 <- ggplot(data = data4, aes(x=factor(data4$size), y=data4$coverage)) +
geom_point(size = 2, aes(shape=data4$dataset, colour=data4$dataset)) +
geom_line(aes(colour=data4$dataset, group=data4$dataset)) +
scale_shape_manual(values = c(0, 2, 8, 15, 19)) +
theme_bw() +
theme(axis.title.y=element_blank(), legend.position = "none", legend.title = element_blank(), plot.margin=unit(c(0.2,0.1,0.2,0),"cm"), axis.ticks.y=element_blank(), axis.text.y = element_blank()) +
xlab("Dataset Cardinality") +
ylab("Coverage (%)") +
ylim(0, 80) +
scale_size_manual(values=c(2,.5,.5,.5,.5,.5)) +
scale_colour_manual(values=cbPalette) +
scale_x_discrete(labels = c("10K", "50K", "100K", "1M"))

grid.arrange(plot1, plot2, plot3, plot4, nrow = 1, widths=c(1.1, 0.95, 0.94, 0.86))
