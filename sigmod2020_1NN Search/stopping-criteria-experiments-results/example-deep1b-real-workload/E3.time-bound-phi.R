# Created by Theophanis Tsandilas, Inria
# Stopping based on Quantile regression to assess an upper bound for the time to find the exact answer (1NN)

# Clean the memory from all objects
rm(list=ls())

require("quantreg")

# General process.
#  1000 or 5000 series are used as the pool of training and testing queries (T = R + S)
# Size of S (testing queries per iteration) N.test = 200 (constant)
# Size of R (training queries) k = 50, 100, 200, 500
# We follow a Monte Carlo validation approach that repeats  N = 100 times the following steps:
# 1. We randomly draw a sample (from T) for the testing set S
# 2. From the remaining series, we randomly draw a sample of size k for the training set R
# 3. We train the model and then test it
# 4. We repeat

dataset <- "deep1b"

k <- 100 # Number of training queries
N <- 20 # Testing iterations

phi <- 0.01 # To vary for other phi values
# phi <- 0.05
# phi <- 0.10

csvfile <- paste("output/k-", k, "-time-phi-", phi, ".csv", sep="")

summary.table <- function(filename){
    data.bsf <- read.csv(file=filename, header=TRUE, sep=",")
    
    data.bsf <- data.bsf[,c(2,3,4,5,6,9,10)]
    # data.bsf$time <- log2(data.bsf$time)
    #data.bsf$totalTime <- log2(data.bsf$totalTime)
    #data.bsf$leavesVisited <- log2(data.bsf$leavesVisited)
    #data.bsf$totalLeavesVisited <- log2(data.bsf$totalLeavesVisited)
    
    # Attention! Remove this if you really want prediction to be performed on the number of visited leaves
    # I have this just for practical reasons to make comparions based on time rather than number of leaves
    data.bsf$leavesVisited <- log2(data.bsf$time)
    data.bsf$totalLeavesVisited <- log2(data.bsf$totalTime)
    
    data.bsf$distance <- sqrt(data.bsf$distance )
    
    best <- data.bsf[data.bsf$answerId=="bestNN",]
    
    merge(data.bsf, best, by="queryId")[,c(1,2,4,5,6,7,3,9)]
}

getTrainingFrame <- function(dat){
    appr <- aggregate(distance.x ~ queryId, data = dat, FUN = max)
    best <- dat[dat$answerId.x=="bestNN",]
    merge(appr, best, by="queryId")
}


pi <- function(dist, lmodel, level = 0.95) {
    newdata = data.frame(distance.x = dist)
    PI <- predict(lmodel, newdata, interval="predict", level = level)
    
    c(PI[1], max(0, PI[2]), min(dist, PI[3]))
}

results <- data.frame(k = integer(), dataset = character(), i = integer(), num = integer(), qid = integer(), typeI = integer(), typeIb = integer(), eq = double(), time = integer(), timeTotal = integer(), percent = double(), distance = double(), stringsAsFactors=FALSE)

#results <- data.frame(k = integer(), dataset = character(), i = integer(), qid = integer(), time = double(), totalTime =  double(), eq = double(), ratio = double(), stringsAsFactors=FALSE)

# Data preparation #################################################
queries.file <- paste("data/", dataset, ".csv", sep="")

# Data preparation #################################################
queries <- summary.table(queries.file)

queries <- queries[queries$distance.x < 10000,]
queries <- queries[queries$distance.y > 0,]

# I divide the max possible time into T quanta
maxi <- 1.2*max(queries$leavesVisited)
quanta <- seq(from = 0, to = maxi, by = maxi/T)

ids <- unique(queries$queryId)

for(i in 1:N){
    training.ids <- sample(ids, k)
    testing.ids <- ids[-training.ids]

    testing <- queries[queries$queryId %in% testing.ids,]
    training <- queries[queries$queryId %in% training.ids,]

    training.summary <- getTrainingFrame(training)[c(2,6)]

    names(training.summary) <- c("distance", "leavesVisited")

    #lmodel <- lm(leavesVisited ~ distance, data = training)
    lmodel <- rq(leavesVisited ~ distance, data = training.summary, tau = 1 - phi)
    slope <- lmodel$coefficients[[2]]
    intercept <- lmodel$coefficients[[1]]

    # Need to add results for training queries
    counter <- 0
    for(qid in training.ids){
        
        query <- training[training$queryId == qid,]
        j <- 1
        counter <- counter + 1
        
        results[nrow(results) + 1, ]  <- list(k, dataset, i, counter, qid, -1, 0, 0, query[j,]$totalTime.x, query[j,]$totalTime.x, 1, query[j,]$distance.y)
    }
    
    
    # And now do the testing!
    for(qid in testing.ids){
        query <- testing[testing$queryId == qid,]

        prediction = query[1,]$distance.x * slope + intercept
        
        counter <- counter + 1
        
        #distance <- query[1,]$distance.x
        #PI <- pi(distance, lmodel, level = 0.9)
        
        t <- 2^prediction

        for(j in 1:nrow(query)){
            line <- query[j,]
            T <- 2^line$totalLeavesVisited.x
            t <- min(t, T)

            if(line$leavesVisited.x > prediction){
                if(j == 1){
                    dist <- line$distance.x
                    t <- 2^line$leavesVisited.x
                }
                else {
                    dist <- query[j -1,]$distance.x
                }
                # STOP here....
                eq <- (dist / line$distance.y) - 1
                if(eq == 0) err <- 0
                else err <- 1
                
                results[nrow(results) + 1, ]  <- list(k, dataset, i, counter, qid, err, err, eq, t, T, (t / T), line$distance.y)

                break;
            } else if(line$answerId.x == "bestNN"){
                results[nrow(results) + 1, ]  <- list(k, dataset, i, counter, qid, 0, 0, 0, t, T, (t / T), line$distance.y)
            }
        }
    }
}

write.csv(results, file=csvfile)
