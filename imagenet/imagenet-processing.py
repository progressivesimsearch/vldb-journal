# Theophanis Tsandilas
# Based on this tutorial: https://www.tensorflow.org/tutorials/images/transfer_learning

# images are downloaded from here: https://www.kaggle.com/c/imagenet-object-localization-challenge/data
# See further instructions here: https://www.tensorflow.org/datasets/catalog/imagenet2012

#See: https://stackoverflow.com/questions/40744700/how-can-i-find-imagenet-data-labels

import matplotlib.pyplot as plt
import numpy as np
import os
import tensorflow as tf

import pandas as pd

from tensorflow.keras.preprocessing import image_dataset_from_directory

# Use dict (dictionary) to map synset to a unique class
# https://stackoverflow.com/questions/15705111/getting-string-from-pandas-series-and-dataframes-in-python/34460966
def getPrimaryClass(dict, synset):
  synonyms = dict[dict.synset==synset].synonyms.tolist()[0]
  return synonyms.split(',')[0] #Return the first only synonym
  
# Use the model to predict the class -- i will not use this one
def predictLabel(img, model):
  predictions = model(img, training=False).numpy()
  props = tf.nn.softmax(predictions).numpy()[0] # Get the probabilities
  return np.where(props == np.amax(props))[0][0] # Find the index of the maximum propability

PATH = os.path.join(os.path.dirname('.'), 'imagenet-dataset')

train_dir = os.path.join(PATH, 'train')
val_dir = os.path.join(PATH, 'val')
# test_dir = os.path.join(PATH, 'test')
data_dir = train_dir # I will focus on this one. Change ti val_dir if needed

BATCH_SIZE = 150
IMG_SIZE = (160, 160)

imagenet_dataset = image_dataset_from_directory(data_dir,
                                             shuffle=False,
                                             batch_size=BATCH_SIZE,
                                             image_size=IMG_SIZE)

synset_names = imagenet_dataset.class_names

# Load the dictionary with the class mappings
DICTIONARY_PATH = os.path.join(PATH, 'synset_mappings.csv')
dictionary = pd.read_csv(DICTIONARY_PATH,sep=';')

# Where to output the embeddings
OUTPUT_PATH = os.path.join(PATH, 'train-embeddings-efficientNetB1.csv')

IMG_SHAPE = IMG_SIZE + (3,)

# Create the base model from the pre-trained model MobileNet V2
#base_model = tf.keras.applications.MobileNetV2(input_shape=IMG_SHAPE,
#                                               include_top=False, # To set to False
#                                               weights='imagenet')
                                               
# Or better use EfficientNetB1 that produces vectors of similar size and is expected to have a higher accuracy
base_model = tf.keras.applications.EfficientNetB1(input_shape=IMG_SHAPE,
                                               include_top=False, # To set to False
                                               weights='imagenet')

# Make sure we don't change the model
base_model.trainable = False

# How to average the feature vectors to create a unique one
global_average_layer = tf.keras.layers.GlobalAveragePooling2D()

count = 0
# For the validation dataset first. Create feature vectors of size 1280
with open(OUTPUT_PATH, 'a') as fout:
  for image_batch, label_batch in imagenet_dataset:
    df = pd.DataFrame(columns=['id', 'label', 'vector'])
    feature_batch = base_model(image_batch)
    feature_batch_average = global_average_layer(feature_batch)
    for index in range(len(image_batch)):
      count += 1
      df = df.append({'id' : count, 'label' : synset_names[label_batch[1].numpy()], 'vector' : feature_batch_average[index].numpy().tolist()}, ignore_index=True)
    print(count)
    df.to_csv(fout, header=False, sep=';', index = False)

#print(feature_batch_average.shape)

# Try predictions
#predictions = base_model(image_batch[:1], training=False).numpy()
#props = tf.nn.softmax(predictions).numpy()[0] # Get the probabilities
# Find the index of the maximum propability
#result = np.where(props == np.amax(props))[0][0]

#predictions = model(x_train[:1]).numpy()
#predictions

#for images, labels in imagenet_dataset.take(1):
#  for i in range(9):
#    ax = plt.subplot(3, 3, i + 1)
#    plt.imshow(images[i].numpy().astype("uint8"))
#    class_real = getPrimaryClass(dictionary,synset_names[labels.numpy()[i]])
#    plt.title(class_real)
#    plt.axis("off")
    
#plt.show()
    # This does not work -- I guess it needs retraining over the last layer
    #img=tf.expand_dims(images[i], 0)
    #class_predicted = getPrimaryClass(dictionary,synset_names[predictLabel(img, base_model)])
    #print(class_real + " --- "+ class_predicted)
