# Theophanis Tsandilas
# Imagenet hierarchy catergorization based on: https://www.biorxiv.org/content/10.1101/2020.07.04.188169v2.full

import os
import pandas as pd
import nltk
from nltk.corpus import wordnet as wn

classes = ["structure", "conveyance", "device", "container", "equipment", "implement", "furnishing", "toiletry", "commodity", "covering", "artifact", "fungus", "fish", "bird", "amphibian", "reptile", "invertebrate", "primate", "ungulate", "plant", "canine", "feline", "carnivore", "mammal", "natural_object", "abstract_entity", "matter", "geological_formation", "person", "entity"]

#class_synsets =

def getLargerClass(word):
  synset = wn.synset(word + '.n.01')
  path = synset.hypernym_paths()[0]

  class_index = len(classes) - 1

  for synset in reversed(path):
    name = synset.lemma_names()[0]
    if(name in classes):
      class_index = classes.index(name)
      break
      
  return class_index

DICTIONARY_PATH = os.path.join(os.path.dirname('.'), 'imagenet-mapped-classes.csv')
df = pd.read_csv(DICTIONARY_PATH,sep=';')
words = df['class1']

class_indices = []
class_names = []
for word in words:
  index = getLargerClass(word)
  class_indices.append(index)
  class_names.append(classes[index])

df['class2'] = class_names

OUTPUT_PATH = os.path.join(os.path.dirname('.'), 'all-classes.csv')
with open(OUTPUT_PATH, 'w') as fout:
  df.to_csv(fout, header=True, sep=';', index = False)

print(df)
