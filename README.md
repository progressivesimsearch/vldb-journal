## ProS: Data Series Progressive k-NN Similarity Search and Classification with Probabilistic Quality Guarantees

This repository presents materials that we used to evaluate our probabilistics models and to generate the graphs of our article submission to the VLDB Journal. 

Note that the code here does not implement the iSAX2+ and DSTree algorithms. The code for the algorithms is provided seperately. 
Also, the datasets that we include here are not the original datasets. They are .csv logs that fully describe the evolution of the search (or classification) when applying each of these algorithms (either with ED or with DTW) to the original datasets. 

Our experimental code (in R) then reads these logs and runs the models a large number of times. This allows us to evaluate our models over a large number of iterations.

The structure of the supplementary matieral is as follows:  
- *sigmod2020_1NN Search.* Code used for the Sigmod2020 paper. This code only handles 1-NN similarity search for the Euclidean distance (ED)
- *datasets.* The new .csv logs for k-NN similarity search and classification.
- *imagenet.* Code used to process the imagenet dataset and generate our embeddings
- *scripts-kNN-Search-ED.* Evaluation experiments and code generating figures for k-NN search with ED. 
- *scripts-kNN-Search-DTW.* Evaluation experiments and code generating figures for k-NN search with DTW.  
- *scripts-classification.* Evaluation experiments and code generating figures for k-NN classification.  

Scripts that we used to evaluate our models have a "E" prefix (e.g., *E1.time-bound-phi.R*). Their output is included under a results folder. 

Scripts that we used to generate our graphs have a "G" prefix (e.g., *G1.time-bound.R*). The use as input the .csv files  in the results folder. 
